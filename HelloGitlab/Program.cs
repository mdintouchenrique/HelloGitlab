﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Reflection;

namespace HelloGitlab {
    class Program {
        static void Main(string[] args) {
            String fileVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion; 
            Console.WriteLine("Hello Gitlab! Version: {0}", fileVersion);
            Console.WriteLine("Press Any Key to Exit...");
            Console.ReadLine();
        }
    }
}
