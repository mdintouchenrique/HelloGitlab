# HelloGitlab Test Application
For Gitlab features and functionality testing purposes.

## Development Environment

* IDE: Visual Studio 2010
* Target Framework: .Net 4.0

Copyright &copy; 2018 MDinTouch US, Inc.
